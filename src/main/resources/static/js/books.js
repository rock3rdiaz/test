/*
 * 
 * Funcion que mediante una peticion ajax adiciona el libro seleccionado al 
 * carrito de compras.
 */
function sendRequest(e) {
	console.info('amount ' + $('#value-' + e.id).val());
	event.preventDefault();
	var data = {
			'amount': $('#amount-' + e.id).val(),
			'book': {
				'id': e.id,
				'value': $('#value-' + e.id).text(),
			}
	};
	$.ajax({
		type : "POST",
		contentType : "application/json",
		url : "/books/api-add-book",
		data : JSON.stringify(data),
		success : function(data) {
			console.log("request completed ...");
			window.location.reload();
		},
		error : function(e) {
			console.error("request error ...");
		}
	});	
}