package com.training.viewmodels;

import com.training.models.BookModel;

public class BookViewModel {

	private BookModel book;
	private int amount;
	
	public BookViewModel() {
		book = new BookModel();
	}

	public BookViewModel(BookModel book) {
		this.book = book;
		this.amount = 0;
	}
	
	public BookViewModel(BookModel book, int amount) {
		this.book = book;
		this.amount = amount;
	}

	public BookModel getBook() {
		return book;
	}

	public void setBook(BookModel book) {
		this.book = book;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}
}
