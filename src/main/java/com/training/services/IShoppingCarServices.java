package com.training.services;

import com.training.models.BookModel;
import com.training.models.ShoppingCarModel;

public interface IShoppingCarServices {

	ShoppingCarModel getShoppingActiveCar(int userId);
	void addBook(ShoppingCarModel shoppingCar, BookModel book, int amount);
	void removeBook(ShoppingCarModel shoppingCar, BookModel book);
	void makeBuy(ShoppingCarModel shoppingCar);
	void makeCancel(ShoppingCarModel shoppingCar);
}
