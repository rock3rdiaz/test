package com.training.services;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.training.models.BookModel;
import com.training.repositories.IBookRepository;

@Service
public class InventaryServices implements IInventaryServices {

	private static final Logger logger = LoggerFactory.getLogger(InventaryServices.class);

	@Autowired
	private IBookRepository bookRepository;

	@Override
	public List<BookModel> getAvailableBooks() {
		return (List<BookModel>) bookRepository.findAll();
	}

	@Override
	public boolean substractBook(int bookId, int amount) {
		Optional<BookModel> optionalBook = bookRepository.findById(bookId);
		if (optionalBook.isPresent()) {
			BookModel book = optionalBook.get();
			int currentAmount = book.getAmount();
			if (amount > currentAmount) {
				logger.error("el numero de existencias disponibles es menor a la cantidad solicitada.");
				return false;
			}
			book.setAmount(currentAmount - amount);
			bookRepository.save(book);
			return true;
		}
		logger.error("Libro no encontrado.");
		return false;
	}

	@Override
	public boolean addBook(int bookId, int amount) {
		Optional<BookModel> optionalBook = bookRepository.findById(bookId);
		if (optionalBook.isPresent()) {
			BookModel book = optionalBook.get();
			int currentAmount = book.getAmount();
			book.setAmount(currentAmount + amount);
			bookRepository.save(book);
			return true;
		}
		logger.error("Libro no encontrado.");
		return false;
	}

}
