package com.training.services;

import java.util.ArrayList;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.training.enums.ShoppingCarStatesEnum;
import com.training.models.BookModel;
import com.training.models.ShoppingCarBookModel;
import com.training.models.ShoppingCarModel;
import com.training.repositories.IShoppingCarBookRepository;
import com.training.repositories.IShoppingCarRepository;

@Service
public class ShoppingCarServices implements IShoppingCarServices {

	@Autowired
	private IInventaryServices inventaryService;

	@Autowired
	private IShoppingCarRepository shoppingCarRepository;

	@Autowired
	private IShoppingCarBookRepository shoppingCarBookRepository;

	@Override
	public ShoppingCarModel getShoppingActiveCar(int userId) {
		return shoppingCarRepository.findByUser_IdAndState(userId, ShoppingCarStatesEnum.IN_PROGRESS.getState());
	}

	public void addBook(ShoppingCarModel shoppingCar, BookModel book, int amount) {
		if (shoppingCar.getShoppingCarItems() == null) {
			shoppingCar.setShoppingCarItems(new ArrayList<>());
		}
		ShoppingCarBookModel item = new ShoppingCarBookModel();
		item.setBook(book);
		item.setShoppingCar(shoppingCar);
		item.setAmount(amount);
		shoppingCar.getShoppingCarItems().add(item);
		if (inventaryService.substractBook(book.getId(), amount)) {
			float total = shoppingCar.getTotal();
			shoppingCar.setTotal(total + ((float) amount * book.getValue()));
			shoppingCarRepository.save(shoppingCar);
		}
	}

	@Override
	public void removeBook(ShoppingCarModel shoppingCar, BookModel book) {
		for (ShoppingCarBookModel item : shoppingCar.getShoppingCarItems()) {
			if (item.getBook().getId() == book.getId()) {
				shoppingCar.getShoppingCarItems().remove(book);
				break;
			}
		}
		// inventaryService.substractBook(book.getId(), amount);
		shoppingCarBookRepository.saveAll(shoppingCar.getShoppingCarItems());
	}

	@Override
	public void makeBuy(ShoppingCarModel shoppingCar) {
		shoppingCar.setState(ShoppingCarStatesEnum.COMPLETED.getState());
		shoppingCarRepository.save(shoppingCar);
		ShoppingCarModel i = new ShoppingCarModel();
		i.setBuyDate(new Date());
		i.setId(shoppingCar.getId() + 1);
		i.setShoppingCarItems(new ArrayList<>());
		i.setState(ShoppingCarStatesEnum.IN_PROGRESS.getState());
		i.setUser(shoppingCar.getUser());
		shoppingCarRepository.save(i);
	}

	@Override
	public void makeCancel(ShoppingCarModel shoppingCar) {
		for (ShoppingCarBookModel item : shoppingCar.getShoppingCarItems()) {
			inventaryService.addBook(item.getBook().getId(), item.getAmount());
		}
		shoppingCar.getShoppingCarItems().clear();
		shoppingCar.setTotal(0.0f);
		shoppingCarRepository.save(shoppingCar);
	}
}
