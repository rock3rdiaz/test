package com.training.services;

import java.util.List;

import com.training.models.BookModel;

public interface IInventaryServices {
	
	/*
	 * Return all {@link BookModel}
	 * 
	 */
	List<BookModel> getAvailableBooks();
	
	boolean substractBook(int bookId, int amount);
	boolean addBook(int bookId, int amount);
}
