package com.training.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.training.services.IShoppingCarServices;
import com.training.viewmodels.BookViewModel;

@RestController
@RequestMapping("/books")
public class BooksRestController {

	private static final Logger logger = LoggerFactory.getLogger(BooksRestController.class);
	
	@Autowired
	private IShoppingCarServices shoppingCarServices;

	@PostMapping("/api-add-book")
	public void addBook(@RequestBody BookViewModel request) {
		shoppingCarServices.addBook(shoppingCarServices.getShoppingActiveCar(1), request.getBook(), request.getAmount());
	}
}
