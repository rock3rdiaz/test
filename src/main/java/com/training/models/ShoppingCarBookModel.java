package com.training.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "shopping_car_book", uniqueConstraints = { @UniqueConstraint(columnNames = { "shopping_car", "book" }) })
public class ShoppingCarBookModel {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Column(nullable = false)
	private int amount;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "shopping_car")
	private ShoppingCarModel shoppingCar;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "book", nullable = false)
	private BookModel book;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	public ShoppingCarModel getShoppingCar() {
		return shoppingCar;
	}

	public void setShoppingCar(ShoppingCarModel shoppingCar) {
		this.shoppingCar = shoppingCar;
	}

	public BookModel getBook() {
		return book;
	}

	public void setBook(BookModel book) {
		this.book = book;
	}
}
