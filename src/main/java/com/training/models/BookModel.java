package com.training.models;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "book")
public class BookModel {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Column(length = 90, unique = true, nullable = false)
	private String name;

	@Column(nullable = false)
	private float value;

	@Column(nullable = false)
	private int amount;

	@Column(length = 100, nullable = true)
	private String image;

	@OneToMany(mappedBy = "book")
	private List<ShoppingCarBookModel> shoppingCarItems;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public float getValue() {
		return value;
	}

	public void setValue(float value) {
		this.value = value;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public List<ShoppingCarBookModel> getShoppingCarItems() {
		return shoppingCarItems;
	}

	public void setShoppingCarItems(List<ShoppingCarBookModel> shoppingCarItems) {
		this.shoppingCarItems = shoppingCarItems;
	}
}
