package com.training.models;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "shopping_car")
public class ShoppingCarModel {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Column(name = "buy_date", nullable = false)
	private Date buyDate;

	@Column(nullable = false)
	private float total;

	@Column(nullable = false)
	private int state;

	@ManyToOne(fetch = FetchType.LAZY)
	private UserModel user;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "shoppingCar", orphanRemoval = true)
	private List<ShoppingCarBookModel> shoppingCarItems;

	/*
	 * Retorna el total a pagar de acuerdo a la formula establecida
	 * 
	 */
	public float getTotal() {
		return total;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getBuyDate() {
		return buyDate;
	}

	public void setBuyDate(Date buyDate) {
		this.buyDate = buyDate;
	}

	public int getState() {
		return state;
	}

	public void setState(int state) {
		this.state = state;
	}

	public UserModel getUser() {
		return user;
	}

	public void setUser(UserModel user) {
		this.user = user;
	}

	public List<ShoppingCarBookModel> getShoppingCarItems() {
		return shoppingCarItems;
	}

	public void setShoppingCarItems(List<ShoppingCarBookModel> shoppingCarItems) {
		this.shoppingCarItems = shoppingCarItems;
	}

	public void setTotal(float total) {
		this.total = total;
	}
}
