package com.training.enums;

public enum ShoppingCarStatesEnum {

	IN_PROGRESS(1),
	COMPLETED(2),
	CANCELED(3);
	
	private int state;
	
	private ShoppingCarStatesEnum(int state) {
		this.state = state;
	}
	
	public int getState() {
		return state;
	}
}
