package com.training.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.training.models.ShoppingCarModel;
import com.training.services.IShoppingCarServices;
import com.training.viewmodels.BookViewModel;

@Controller
@RequestMapping("/shopping-car")
public class ShoppingCarController extends BaseController {

	@Value("${messages.title}")
	private String title;

	@Value("${messages.shoppingcar.details}")
	private String shoppingCarDetails;

	private List<BookViewModel> books;

	@Autowired
	private IShoppingCarServices shoppingCarService;

	@GetMapping("/detail")
	public String showDetail(Model model) {
		ShoppingCarModel shoppingCar = shoppingCarService.getShoppingActiveCar(1);
		model.addAttribute("shoppingCar", shoppingCar);
		model.addAttribute("title", title);
		model.addAttribute("shoppingCarDetails", shoppingCarDetails);
		model.addAttribute("books", getViewModelFromShoppingCar(shoppingCar.getShoppingCarItems()));
		return "shopping-car-detail";
	}

	@GetMapping("/buy")
	public String toPay() {
		// popup de confirmacion
		ShoppingCarModel shoppingCar = shoppingCarService.getShoppingActiveCar(1);
		shoppingCarService.makeBuy(shoppingCar);
		return "redirect:/books/list";
	}

	@GetMapping("/cancel")
	public String toCancel() {
		// popup de confirmacion
		ShoppingCarModel shoppingCar = shoppingCarService.getShoppingActiveCar(1);
		shoppingCarService.makeCancel(shoppingCar);
		return "redirect:/books/list";
	}
}
