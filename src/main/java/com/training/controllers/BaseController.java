package com.training.controllers;

import java.util.ArrayList;
import java.util.List;

import com.training.models.BookModel;
import com.training.models.ShoppingCarBookModel;
import com.training.viewmodels.BookViewModel;

public class BaseController {

	/**
	 * get ViewModel assocated to form view.
	 * 
	 * @param books
	 * @return
	 */
	protected List<BookViewModel> getViewModelFromBooks(List<BookModel> books) {
		List<BookViewModel> viewModel = new ArrayList<>();
		for(BookModel b : books) {
			viewModel.add(new BookViewModel(b));
		}
		return viewModel;
	}
	
	/**
	 * get ViewModel assocated to form view.
	 * 
	 * @param books
	 * @return
	 */
	protected List<BookViewModel> getViewModelFromShoppingCar(List<ShoppingCarBookModel> shoppingCarDetails) {
		List<BookViewModel> viewModel = new ArrayList<>();
		for(ShoppingCarBookModel i : shoppingCarDetails) {
			viewModel.add(new BookViewModel(i.getBook(), i.getAmount()));
		}
		return viewModel;
	}
}
