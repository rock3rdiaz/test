package com.training.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.training.rest.BooksRestController;
import com.training.services.IInventaryServices;
import com.training.services.IShoppingCarServices;

@Controller
@RequestMapping("/books")
public class BooksController extends BaseController {

	private static final Logger logger = LoggerFactory.getLogger(BooksRestController.class);

	@Value("${messages.books.list}")
	private String bookListTitle;

	@Value("${messages.title}")
	private String title;

	@Value("${messages.books.availables}")
	private String booksAvailables;

	@Value("${messages.books.goshoppingcar}")
	private String goShoppingCar;

	@Autowired
	private IInventaryServices inventaryServices;

	@Autowired
	private IShoppingCarServices shoppingCarServices;

	@GetMapping("/list")
	public String showBooks(Model model) {
		shoppingCarServices.getShoppingActiveCar(1);
		model.addAttribute("bookListTitle", bookListTitle);
		model.addAttribute("title", title);
		model.addAttribute("goShoppingCar", goShoppingCar);
		model.addAttribute("booksAvailables", booksAvailables);
		model.addAttribute("books", getViewModelFromBooks(inventaryServices.getAvailableBooks()));
		return "book-list";
	}
}
