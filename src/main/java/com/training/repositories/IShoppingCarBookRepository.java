package com.training.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.training.models.ShoppingCarBookModel;

@Repository
public interface IShoppingCarBookRepository extends CrudRepository<ShoppingCarBookModel, Integer> {

}
