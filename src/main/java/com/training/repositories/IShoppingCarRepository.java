package com.training.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.training.models.ShoppingCarModel;

@Repository
public interface IShoppingCarRepository extends CrudRepository<ShoppingCarModel, Integer> {

	List<ShoppingCarModel> findByState(int state);
	ShoppingCarModel findByUser_IdAndState(int id, int state);
}
