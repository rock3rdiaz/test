package com.training.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.training.models.BookModel;

@Repository
public interface IBookRepository extends CrudRepository<BookModel, Integer> {

}
