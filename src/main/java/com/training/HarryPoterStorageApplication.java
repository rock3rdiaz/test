package com.training;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HarryPoterStorageApplication {

	public static void main(String[] args) {
		SpringApplication.run(HarryPoterStorageApplication.class, args);
	}
}
